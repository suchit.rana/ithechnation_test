from django.shortcuts import render, redirect
from .models import register
# Create your views here.

import logging

logger = logging.getLogger('django')


def index(request):
    try:
        data = register.objects.all()  # set default filter for deleted_at = NULL
        return render(request, 'index.html', {'data': data})
    except BaseException as e:
        logger.error('INDEX ERROR : ')
        logger.error(e)


def add(request):
    print(request.method)
    if request.method == "POST":
        name = request.POST.get('name')
        address = request.POST.get('address')
        email = request.POST.get('email')
        phno = request.POST.get('phno')
        gender = request.POST.get('gender')
        try:
            register_data = register()
            register_data.name = name
            register_data.email = email
            register_data.address = address
            register_data.gender = gender
            register_data.mobile_no = phno
            register_data.save()
            return redirect('index')
        except BaseException as e:
            logger.error('ADD ERROR : ')
            logger.error(e)

    else:
        return render(request, 'add.html')


def edit(request, id=0):
        id = int(id)
        if request.method == "POST":
            name = request.POST.get('name')
            address = request.POST.get('address')
            email = request.POST.get('email')
            phno = request.POST.get('phno')
            gender = request.POST.get('gender')
            try:
                register_data = register.objects.get(id=id)
                register_data.name = name
                register_data.email = email
                register_data.address = address
                register_data.gender = gender
                register_data.mobile_no = phno
                register_data.save()
                return redirect('index')
            except BaseException as e:
                logger.error('EDIT ERROR : ')
                logger.error(e)
        else:
            data = register.objects.get(id=id)
            return render(request, 'edit.html', {'data': data})


def delete(request, id):
    try:
        id = id
        technologies = register.objects.get(id=id)
        technologies.delete()
        return redirect('index')
    except BaseException as e:
        logger.error('DELETE ERROR : ')
        logger.error(e)
