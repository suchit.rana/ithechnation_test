from django.db import models
from django.utils import timezone


# Create your models here.

class register(models.Model):
    name = models.CharField(max_length=255, default=None, blank=True, null=True)
    address =  models.CharField(max_length=255, default=None, blank=True, null=True)
    email =  models.EmailField(max_length=255,default=None, blank=True, null=True)
    gender = models.IntegerField(default=None, blank=True, null=True)
    mobile_no = models.CharField(max_length=20, default=None, blank=True, null=True)

    created_at = models.DateTimeField(editable=False)
    deleted_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField()


    def save(self, *args, **kwargs):
        if not self.id:
            self.created_at = timezone.now()
        self.updated_at = timezone.now()


        return super(register, self).save(*args, **kwargs)

    def delete(self):
        self.deleted_at = timezone.now()
        self.save()
